from circus.commands.base import Command
from circus.exc import ArgumentError


class ReadFile(Command):
    """\
        Read a file and print its contents
        ==================================

        This command reads a file at <filepath>, optionally skipping <skiplines>
        lines while reading it.

        ZMQ Message
        -----------

        ::

            {
                "command": "readfile",
                "properties": {
                    "filepath": "<filepath>"
                    "skiplines": <skiplines>
                }
            }

        The response returns a list of lines as the "lines" property::

            { "status": "ok", "lines": ["<line1>", "<line2>"], "time", "timestamp" }

        Command line
        ------------

        ::

            $ circusctl readfile <filepath> [<skiplines>]

        Options
        +++++++

        - <filepath>: path to the file being read
        - <skiplines>: the number of lines to skip

    """

    name = 'readfile'
    options = [('skiplines', 'skiplines', 0, 'How many lines to skip')]
    properties = ['filepath']

    def message(self, *args, **opts):
        if len(args) < 1:
            raise ArgumentError("Invalid number of arguments")
        opts['filepath'] = args[0]
        if len(args) > 1:
            opts['skiplines'] = int(args[1])
        return self.make_message(**opts)

    def execute(self, arbiter, props):
        path = props.get('filepath')
        lines = []
        skiplines = props.get('skiplines', 0)

        with open(path, 'r') as f:
            line_number = 0
            for line in f:
                line_number += 1
                if line_number > skiplines:
                    # Remove the trailing newline
                    lines.append(line.rstrip('\n'))
        return {'lines': lines}

    def console_msg(self, msg):
        if msg.get('status') == 'ok':
            return '\n'.join(msg.get('lines', []))
        return self.console_error(msg)
